#pragma once
#include "De.h"
#include "cst.h"

class Joueur {
  public:
    Joueur();
    short LanceDes(De &unDe, De &deuxDe);
    short GetPosition();
    void  Crediter(short);       // Créditer une somme sur le portefeuille
    short Solde();               // Récupérer le solde du portefeuille
    short position;

  private:
    int   portefeuille;
};

#pragma once
#include <iostream>
#include <string>

using namespace std;

class Case {

  public:

    Case();
    Case(short position, string type);
    ~Case();
    virtual void Afficher();
    void         SetType(string type);
    string       GetType();
    void         SetPosition(short position);
    short        GetPosition();
   

  protected:

      string type;
      short  position;
      
};

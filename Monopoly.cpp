#include <cstdlib>
#include <iostream>
#include "Case.h"
#include "De.h"
#include "Ville.h"
#include "Joueur.h"
#include "Depart.h"

using namespace std;

int main(int argc, char *argv[]) {
   

    Case *pCases[40];
    pCases[0] = new Depart;
    pCases[1] = new Ville(1, "VILNIUS", 60, 2);
    pCases[2] = new Case(2, "rien");
    pCases[3] = new Ville(3, "RIGA", 60, 4);
    pCases[4] = new Case(4, "rien");
    pCases[5] = new Case(5, "rien");
    pCases[6] = new Ville(6, "SOFIA", 100, 6);
    pCases[7] = new Case(7, "rien");
    pCases[8] = new Ville(8, "BUCAREST", 100, 6);
    pCases[9] = new Ville(9, "VARSOVIE", 120, 8);
    pCases[10] = new Case(10, "rien");
    pCases[11] = new Ville(11, "BUDAPEST", 140, 10);
    pCases[12] = new Case(12, "rien");
    pCases[13] = new Ville(13, "BERNE", 140, 10);
    pCases[14] = new Ville(14, "HELSINKI", 160, 12);
    pCases[15] = new Case(15, "rien");
    pCases[16] = new Ville(16, "STOCKHOLM", 180, 14);
    pCases[17] = new Case(17, "rien");
    pCases[18] = new Ville(18, "VIENNE", 180, 14);
    pCases[19] = new Ville(19, "LISBONNE", 200, 16);
    pCases[20] = new Case(20, "rien");
    pCases[21] = new Ville(21, "MADRID", 220, 18);
    pCases[22] = new Case(22, "rien");
    pCases[23] = new Ville(23, "ATHENES", 220, 18);
    pCases[24] = new Ville(24, "DUBLIN", 240, 20);
    pCases[25] = new Case(25, "rien");
    pCases[26] = new Ville(26, "COPENHAGUE", 260, 22);
    pCases[27] = new Ville(27, "LONDRES", 260, 22);
    pCases[28] = new Case(28, "rien");
    pCases[29] = new Ville(29, "LUXEMBOURG", 280, 24);
    pCases[30] = new Case(30, "rien");
    pCases[31] = new Ville(31, "BRUXELLES", 300, 26);
    pCases[32] = new Ville(32, "AMSTERDAM", 300, 26);
    pCases[33] = new Case(33, "rien");
    pCases[34] = new Ville(34, "ROME", 320, 28);
    pCases[35] = new Case(35, "rien"); 
    pCases[36] = new Case(36, "rien");
    pCases[37] = new Ville(37, "BERLIN", 350, 35);
    pCases[38] = new Case(38, "rien");
    pCases[39] = new Ville(39, "PARIS", 400, 50);
    for (int i = 0; i < 40; i++) {
        //cout << i << " ";
        pCases[i]->Afficher();
    }

 

 srand((unsigned)time(0));
    {

        // Instanciation des deux dés
        De deUn;
        De deDeux;

        // incrementation des Joueur
        Joueur Louis;
        Joueur Eloi;

        cout << "Bienvenue dans le Monopoly" << endl;

        for (int i = 0; i < 10; i++) { // lancer de dés 10 fois

            // Pierre lance les dés
            short lancer = Eloi.LanceDes(deUn, deDeux);

            cout << "Eloi a lancer les de et a fais " << lancer << endl;

            cout << "Il est sur la case " << Eloi.position << endl;

            // Paul lance les dés
            short lancertest = Louis.LanceDes(deUn, deDeux);

            cout << "Louis a lancer les de et a fais " << lancertest << endl;

            cout << "Il est sur la case " << Louis.position << endl;
        }
    }
}

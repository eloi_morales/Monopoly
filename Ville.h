#pragma once
#include "Case.h"

class Ville : public Case {

  public:

    Ville();
    ~Ville();
    Ville(short position, string nom, short valeurVille, short loyerNu);
    void Afficher();

  private:
    short position;
    string nom;
    int loyerNu;
    int valeurVille;
};

#include <iostream>
#include "Ville.h"
#include "Case.h"

using namespace std;

Ville::Ville(): Case() {

}
Ville::~Ville(){

}
Ville::Ville(short position, string nom, short valeurVille, short loyerNu) : Case(position, nom) { 
    this->position = position;
    this->nom = nom;
    this->loyerNu = loyerNu;
    this->valeurVille = valeurVille;

}

void Ville::Afficher() {
    cout << nom << " en position : " << position << ", de prix de vente : " << valeurVille << ", et de loyer : " << loyerNu << endl;
}

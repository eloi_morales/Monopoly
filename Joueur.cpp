#include "Joueur.h"
#include "De.h"
#include "cst.h"
#include <cstdlib>
#include <iostream>

using namespace std;

short Joueur::LanceDes(De &rUnDe, De &rDeuxDe) {
    // TODO
    return 0;
}

Joueur::Joueur() {
    position = 0;                // case départ
    portefeuille = PORTEFEUILLE; // tout le monde commence avec la même somme
}

short Joueur::GetPosition() { return position; }

void Joueur::Crediter(short aSomme) { // Créditer une somme sur le portefeuille
    portefeuille += aSomme;
}

short Joueur::Solde() { // Récupérer le Solde du portefeuille
    return portefeuille;
}
